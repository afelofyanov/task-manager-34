package ru.tsc.felofyanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.request.TaskClearRequest;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK CLEAR]");

        @NotNull final TaskClearRequest request = new TaskClearRequest(getToken());
        getServiceLocator().getTaskEndpoint().clearTask(request);
    }
}
