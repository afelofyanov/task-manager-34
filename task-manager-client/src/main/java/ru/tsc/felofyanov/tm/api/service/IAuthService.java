package ru.tsc.felofyanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.model.User;

public interface IAuthService {

    void login(@Nullable String login, @Nullable String password);

    void logout();

    @Nullable
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @Nullable
    User getUser();

    @NotNull
    String getUserId();

    boolean isAuth();

    void checkRoles(@Nullable Role[] roles);

    @NotNull
    User check(@Nullable String login, @Nullable String password);
}
