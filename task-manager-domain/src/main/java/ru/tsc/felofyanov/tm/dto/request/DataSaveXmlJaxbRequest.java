package ru.tsc.felofyanov.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataSaveXmlJaxbRequest extends AbstractUserRequest {

    public DataSaveXmlJaxbRequest(@Nullable String token) {
        super(token);
    }
}
