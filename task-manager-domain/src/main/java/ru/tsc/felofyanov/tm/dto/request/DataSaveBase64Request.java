package ru.tsc.felofyanov.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataSaveBase64Request extends AbstractUserRequest {

    public DataSaveBase64Request(@Nullable String token) {
        super(token);
    }
}
