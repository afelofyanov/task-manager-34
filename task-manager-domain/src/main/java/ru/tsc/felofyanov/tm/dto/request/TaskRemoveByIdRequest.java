package ru.tsc.felofyanov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class TaskRemoveByIdRequest extends AbstractIdRequest {

    public TaskRemoveByIdRequest(@Nullable String token, @Nullable String id) {
        super(token, id);
    }
}
