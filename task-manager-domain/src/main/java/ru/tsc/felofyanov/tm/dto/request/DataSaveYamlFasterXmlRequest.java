package ru.tsc.felofyanov.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataSaveYamlFasterXmlRequest extends AbstractUserRequest {

    public DataSaveYamlFasterXmlRequest(@Nullable String token) {
        super(token);
    }
}
