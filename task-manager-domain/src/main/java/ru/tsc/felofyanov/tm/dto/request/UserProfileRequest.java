package ru.tsc.felofyanov.tm.dto.request;


import org.jetbrains.annotations.Nullable;

public final class UserProfileRequest extends AbstractUserRequest {
    public UserProfileRequest(@Nullable String token) {
        super(token);
    }
}
