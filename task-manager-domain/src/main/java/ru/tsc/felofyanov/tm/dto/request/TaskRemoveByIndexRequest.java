package ru.tsc.felofyanov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class TaskRemoveByIndexRequest extends AbstractIndexRequest {

    public TaskRemoveByIndexRequest(@Nullable String token, @Nullable Integer index) {
        super(token, index);
    }
}
