package ru.tsc.felofyanov.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataSaveBackupRequest extends AbstractUserRequest {

    public DataSaveBackupRequest(@Nullable String token) {
        super(token);
    }
}
