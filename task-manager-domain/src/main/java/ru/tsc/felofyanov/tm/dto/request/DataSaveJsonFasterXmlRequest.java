package ru.tsc.felofyanov.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataSaveJsonFasterXmlRequest extends AbstractUserRequest {

    public DataSaveJsonFasterXmlRequest(@Nullable String token) {
        super(token);
    }
}
