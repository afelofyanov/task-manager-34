package ru.tsc.felofyanov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class ProjectGetByIdRequest extends AbstractIdRequest {

    public ProjectGetByIdRequest(@Nullable String token, @Nullable String id) {
        super(token, id);
    }
}
