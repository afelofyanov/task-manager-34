package ru.tsc.felofyanov.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataLoadBase64Request extends AbstractUserRequest {

    public DataLoadBase64Request(@Nullable String token) {
        super(token);
    }
}
