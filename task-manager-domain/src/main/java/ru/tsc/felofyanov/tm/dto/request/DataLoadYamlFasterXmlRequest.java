package ru.tsc.felofyanov.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataLoadYamlFasterXmlRequest extends AbstractUserRequest {

    public DataLoadYamlFasterXmlRequest(@Nullable String token) {
        super(token);
    }
}
