package ru.tsc.felofyanov.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataLoadBinaryRequest extends AbstractUserRequest {

    public DataLoadBinaryRequest(@Nullable String token) {
        super(token);
    }
}
