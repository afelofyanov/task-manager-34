package ru.tsc.felofyanov.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataSaveBinaryRequest extends AbstractUserRequest {

    public DataSaveBinaryRequest(@Nullable String token) {
        super(token);
    }
}
