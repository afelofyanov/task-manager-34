package ru.tsc.felofyanov.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataLoadJsonJaxbRequest extends AbstractUserRequest {

    public DataLoadJsonJaxbRequest(@Nullable String token) {
        super(token);
    }
}
