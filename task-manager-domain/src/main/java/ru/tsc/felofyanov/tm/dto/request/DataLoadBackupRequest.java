package ru.tsc.felofyanov.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataLoadBackupRequest extends AbstractUserRequest {

    public DataLoadBackupRequest(@Nullable String token) {
        super(token);
    }
}
