package ru.tsc.felofyanov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class TaskGetByIndexRequest extends AbstractIndexRequest {

    public TaskGetByIndexRequest(@Nullable String token, @Nullable Integer index) {
        super(token, index);
    }
}
