package ru.tsc.felofyanov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public abstract class AbstractIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public AbstractIndexRequest(@Nullable String token, @Nullable Integer index) {
        super(token);
        this.index = index;
    }
}
