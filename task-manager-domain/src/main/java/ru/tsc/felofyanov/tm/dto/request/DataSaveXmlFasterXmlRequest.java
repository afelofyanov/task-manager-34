package ru.tsc.felofyanov.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataSaveXmlFasterXmlRequest extends AbstractUserRequest {

    public DataSaveXmlFasterXmlRequest(@Nullable String token) {
        super(token);
    }
}
