package ru.tsc.felofyanov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class TaskGetByIdRequest extends AbstractIdRequest {

    public TaskGetByIdRequest(@Nullable String token, @Nullable String id) {
        super(token, id);
    }
}
