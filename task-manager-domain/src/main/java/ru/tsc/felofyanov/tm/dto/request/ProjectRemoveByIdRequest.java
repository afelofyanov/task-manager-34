package ru.tsc.felofyanov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class ProjectRemoveByIdRequest extends AbstractIdRequest {

    public ProjectRemoveByIdRequest(@Nullable String token, @Nullable String id) {
        super(token, id);
    }
}
