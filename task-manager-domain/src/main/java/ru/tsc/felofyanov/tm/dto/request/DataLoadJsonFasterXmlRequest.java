package ru.tsc.felofyanov.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataLoadJsonFasterXmlRequest extends AbstractUserRequest {

    public DataLoadJsonFasterXmlRequest(@Nullable String token) {
        super(token);
    }
}
