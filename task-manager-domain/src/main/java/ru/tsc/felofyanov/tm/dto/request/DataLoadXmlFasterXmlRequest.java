package ru.tsc.felofyanov.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataLoadXmlFasterXmlRequest extends AbstractUserRequest {

    public DataLoadXmlFasterXmlRequest(@Nullable String token) {
        super(token);
    }
}
