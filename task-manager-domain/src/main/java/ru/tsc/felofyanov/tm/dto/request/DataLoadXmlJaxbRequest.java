package ru.tsc.felofyanov.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataLoadXmlJaxbRequest extends AbstractUserRequest {

    public DataLoadXmlJaxbRequest(@Nullable String token) {
        super(token);
    }
}
