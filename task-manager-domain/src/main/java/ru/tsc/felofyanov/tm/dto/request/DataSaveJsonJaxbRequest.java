package ru.tsc.felofyanov.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataSaveJsonJaxbRequest extends AbstractUserRequest {

    public DataSaveJsonJaxbRequest(@Nullable String token) {
        super(token);
    }
}
