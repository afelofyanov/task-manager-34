package ru.tsc.felofyanov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class TaskUpdateByIndexRequest extends AbstractIndexRequest {

    @Nullable
    private String name;

    @Nullable
    private String description;

    public TaskUpdateByIndexRequest(
            @Nullable String token,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    ) {
        super(token, index);
        this.name = name;
        this.description = description;
    }
}
